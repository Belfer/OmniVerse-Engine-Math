#pragma once

namespace math {
template <typename T> constexpr T pi() {
  return static_cast<T>(3.14159265358979323846);
}
}
